This README file contains information on the contents of the meta-oasis layer.

Please see the corresponding sections below for details.


Patches
=======

Maintainer: David Dlouhý <david.dlouhy@tul.cz>

Table of Contents
=================

  I. Adding the meta-oasis layer to your build
 II. Add yours authorized_keys


I. Adding the meta-oasis layer to your build
=================================================

Run `bitbake-layers add-layer meta-oasis`

II. Add yours authorized_keys
========

Run `cat ~/.id_rsa.pub > recipes-authorizedkeys/authorizedkeys/files/authorized_keys`
