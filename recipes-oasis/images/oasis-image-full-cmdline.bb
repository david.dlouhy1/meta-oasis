LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

require recipes-extended/images/core-image-full-cmdline.bb

IMAGE_FEATURES += "package-management"
IMAGE_INSTALL:append = " vim monitor authorizedkeys"