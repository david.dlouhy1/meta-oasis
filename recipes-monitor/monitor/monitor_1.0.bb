DESCRIPTION = "Add Node Exporter"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = "https://github.com/prometheus/node_exporter/releases/download/v1.8.0/node_exporter-1.8.0.linux-amd64.tar.gz"
SRC_URI[md5sum] = "b7b5ebba693db74ac32de301ba2e654f"
SRC_URI[sha256sum] = "c184e5dd98d518ac468339a9e073c233f777e0948a18862dd88e3f1bdcdf1438"
SRC_URI:append = " file://node_exporter.service "
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "node_exporter.service"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/usr/bin
    install -m 0755 ${S}/node_exporter-1.8.0.linux-amd64/node_exporter ${D}/usr/bin

    install -d ${D}/${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/node_exporter.service ${D}/${systemd_unitdir}/system
    
}

FILES:${PN} += "/lib /lib/systemd/"
FILES_${PN} += "/usr/bin/node_exporter ${systemd_unitdir}/system/node_exporter.service"

do_install:append() {
  install -d ${D}/${systemd_unitdir}/system
  install -m 0644 ${WORKDIR}/node_exporter.service ${D}/${systemd_unitdir}/system
}