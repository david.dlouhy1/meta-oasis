DESCRIPTION = "Add Node Exporter"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI += " file://authorized_keys "

S = "${WORKDIR}"

do_install() {
    install -d -m 0700 ${D}/home/root/.ssh
    install -m 0644 ${S}/authorized_keys ${D}/home/root/.ssh
}

FILES:${PN} += "/home /home/root /home/root/.ssh/ /home/root/.ssh/authorized_keys"

